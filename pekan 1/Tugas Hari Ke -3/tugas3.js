//soal 1 dan jawabannya
//buatlah variabel-variabel seperti di bawah ini
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var besar = kataKeempat.toUpperCase();

console.log(kataPertama + " " + kataKedua + " " + kataKetiga + " " + besar);


//soal 2 dan jawabannya
//ubah lah variabel diatas ke dalam integer dan lakukan jumlahkan semua variabel dan tampilkan dalam output
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var kataPertamaInt = Number.parseInt(kataPertama);
var kataKeduaInt = Number.parseInt(kataKedua);
var kataKetigaInt = Number.parseInt(kataKetiga);
var kataKeempatInt = Number.parseInt(kataKeempat);

console.log(kataPertamaInt + kataKeduaInt + kataKetigaInt + kataKeempatInt);


//soal 3 dan jawabannya
//selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 15); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);


//soal 4 dan jawabannya
//isi variabel tersebut dengan angka dari 0 sampai 100. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
var nilai = 85;

if (nilai >= 80 && nilai < 100)
  console.log('Selamat kamu dapat Indek A');
else if (nilai >= 70 && nilai < 80)
  console.log('Selamat kamu dapat Indek B')
else if (nilai >= 60 && nilai < 70)
  console.log('Selamat kamu dapat Indek C')
else if (nilai >= 50 && nilai < 60)
  console.log('Selamat kamu dapat Indek D')
else
  console.log('Selamat kamu dapat Indek E')


//soal 5 dan jawabannya
//ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)
var tanggal = 15;
var bulan = 2;
var tahun = 1995;

switch (bulan) {
  case 1:
    console.log("Januari");
    break;
  case 2:
    console.log(tanggal + " " + "Februari" + " " + tahun);
    break;
  case 3:
    console.log("Maret");
    break;
  default:
    console.log("Input hanya bulan 1 - 3");
}