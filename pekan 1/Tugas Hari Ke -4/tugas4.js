//soal 1
//Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax while. 
//Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur. 
//Jangan lupa tampilkan di console juga judul ‘LOOPING PERTAMA’ dan ‘LOOPING KEDUA’.”
var nilai = 0;

console.log('LOOPING PERTAMA');
while (nilai < 20) {
  nilai += 2;
  console.log(nilai + ' - I love coding');
}

var nilai2 = 22

console.log('LOOPING KEDUA');
while (nilai2 > 2) {
  nilai2 -= 2;
  console.log(nilai2 + ' - I will become a frontend developer');
}

console.log('\n');


//soal 2
// Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax
// for.Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu:
//   SYARAT:
//   A.Jika angka ganjil maka tampilkan Santai
// B.Jika angka genap maka tampilkan Berkualitas
// C.Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
for (var angka = 1; angka < 21; angka++) {
  if (angka % 2 == 0) {
    console.log(angka + ' - Berkualitas');
  } else if (angka % 3 == 0) {
    console.log(angka + ' - I love Coding');
  } else {
    console.log(angka + ' - Santai');
  }
}

console.log('\n');

//soal 3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7. 
// Looping boleh menggunakan syntax apa pun (while, for, do while).   
var tanda = '';
var angka = 0;

while (angka < 7) {
  tanda += '#'
  angka++;
  console.log(tanda);
}

console.log('\n');

//soal 4
// buatlah variabel seperti di bawah ini
var kalimat = "saya sangat senang belajar javascript"
var nama = kalimat.split(" ");

console.log(nama);

console.log('\n');

//soal 5
// buatlah variabel seperti di bawah ini
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();

for (let index = 0; index < daftarBuah.length; index++) {
  const element = daftarBuah[index];
  console.log(element);
}