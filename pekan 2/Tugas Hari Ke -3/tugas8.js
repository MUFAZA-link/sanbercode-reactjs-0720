// Soal 1
//luas lingkaran
let luasLingkaran = (r1, r2) => {
  const phi = 3.14
  return phi * r1 * r2
}

console.log(luasLingkaran(35, 35))

console.log('\n');

//keliling lingkaran
let kelilingLingkaran = (r) => {
  const phi = 3.14
  return (2 * phi) * r
}

console.log(kelilingLingkaran(40))

console.log('\n');

// Soal 2
let kalimat = () => {

}
const kataSatu = 'Saya adalah seorang'
const kataDua = 'frontend developer'

const perkenalan = `${kataSatu} ${kataDua}`
console.log(perkenalan);

console.log('\n');

// Soal 3
let book = class book {
  constructor(name, totalPage, price) {
    this.name = name;
    this.totalPage = totalPage;
    this.price = price;
  }
}
class komik extends book {
  constructor(name, totalPage, price, isColorfulll) {
    super(name, totalPage, price);
    this.isColorfulll = isColorfulll;
  }
}

let book1 = new book("Unity", 200, 100000);
let komik1 = new komik("One Piece", 200, 500000, true);

console.log(`Nama Buku : ${book1.name}, total halaman: ${book1.totalPage}, harga: ${book1.price}`);
console.log(`Nama Komik : ${komik1.name}, total halaman: ${komik1.totalPage}, harga: ${komik1.price}, halaman berwarna: ${komik1.isColorfulll}`);