// soal 1
var DaftarPeserta = {
  nama: "Asep",
  jeniskelamin: "laki-laki",
  hobi: "baca buku",
  tahunlahir: 1992
};

console.log(DaftarPeserta.nama, DaftarPeserta.jeniskelamin, DaftarPeserta.hobi, DaftarPeserta.tahunlahir);

console.log('\n');

// soal 2
var buah = [{
    nama: "strawberry",
    warna: "merah",
    biji: "tidak",
    harga: 9000
  }, {
    nama: "jeruk",
    warna: "orange",
    biji: "ada",
    harga: 8000
  }, {
    nama: "semangka",
    warna: "hijau & merah",
    biji: "ada",
    harga: 10000
  },
  {
    nama: "pisang",
    warna: "kuning",
    biji: "tidak",
    harga: 5000
  }
]
var list = buah.filter(buah => buah.nama == 'strawberry');
console.log('1. Nama:' + " " + list[0].nama)
console.log(" " + 'Warna:' + " " + list[0].warna);
console.log(" " + 'ada biji:' + " " + list[0].biji);
console.log(" " + 'harga:' + " " + list[0].harga);

console.log('\n');

// soal 3
function dataFilm(nama, durasi, genre, tahun) {
  return 'nama film:' + " " + nama + " " + 'durasi:' + " " + durasi + " " + 'genre:' + " " + genre + " " + 'tahun terbit:' + " " + tahun
}
var nama = "Iron Man 3"
var durasi = "131 menit"
var genre = "Action"
var tahun = 2013

var film = dataFilm(nama, durasi, genre, tahun)
console.log(film)

console.log('\n');

// soal 4
class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = "false";
  }
}

var sheep = new Animal("kambing");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\n');

// Release 1
class Ape extends Animal {
  constructor(name) {
    super(name);
    this.legs = 2;
  }

  yell() {
    return "Auooo";
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }

  jump() {
    return "hop hop";
  }
}


var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"

console.log('\n');

// soal 5
class Clock {
  constructor({
    template
  }) {
    var timer;

    function render() {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;

      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

      console.log(output);
    }

    this.stop = function () {
      clearInterval(timer);
    };

    this.start = function () {
      render();
      timer = setInterval(render, 1000);
    };
  }


}

var clock = new Clock({
  template: 'h:m:s'
});
clock.start();