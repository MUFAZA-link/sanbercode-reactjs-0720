var readBooksPromise = require('./promise.js')

var books = [{
    name: 'LOTR',
    timeSpent: 3000
  },
  {
    name: 'Fidas',
    timeSpent: 2000
  },
  {
    name: 'Kalkulus',
    timeSpent: 4000
  }
]

readBooksPromise(10000, books[1])
  .then(results => {
    console.log('hasil ', results)
  })
  .catch(error => {
    console.log('error ', error)
  })

// async function callPromise() {
//   try {
//     const readBooks = await readBooksPromise(10000, books[2]);
//     console.log(readBooks)
//   } catch (error) {
//     console.log('error', error)
//   }
// }
// callPromise();