// Soal 1
// di index.js
var readBooks = require('./callback.js')

var books = [{
    name: 'LOTR',
    timeSpent: 3000
  },
  {
    name: 'Fidas',
    timeSpent: 2000
  },
  {
    name: 'Kalkulus',
    timeSpent: 4000
  }
]
books.forEach(sisaWaktu => readBooks(10000, sisaWaktu, (callbackFn) => {
  console.log(callbackFn)
}))
// Tulis code untuk memanggil function readBooks di sini