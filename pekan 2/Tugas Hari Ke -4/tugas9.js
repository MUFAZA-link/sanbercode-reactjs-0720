//soal 1
let name = "William"

const fullName = {
  name,
  lastName: "Imoh"
}

console.log(fullName);

console.log('\n');

//soal 2
let newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {
  firstName,
  lastName,
  destination,
  occupation,
} = newObject

console.log(firstName, lastName, destination, occupation)

console.log('\n');

//soal 3
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]

console.log(combined)